FROM node:17-alpine3.12
COPY . .
RUN npm install
ENTRYPOINT ["node","app.js"]

